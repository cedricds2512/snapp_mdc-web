import {MDCRipple} from '@material/ripple';

const iconButtonRipple = new MDCRipple(document.querySelector('.test-button'));
iconButtonRipple.unbounded = true;