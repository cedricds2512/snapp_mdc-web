import {MDCTopAppBar} from '@material/top-app-bar';
import {MDCDrawer} from "@material/drawer";
import {MDCTabBar} from '@material/tab-bar';
import {MDCTab} from '@material/tab';
import L from './leaflet';
import './HomePage';
import {MDCRipple} from '@material/ripple';


// Instantiation
const topAppBarElement = document.querySelector('.mdc-top-app-bar');
const topAppBar = new MDCTopAppBar(topAppBarElement);
const drawer = MDCDrawer.attachTo(document.querySelector('.mdc-drawer'));
const listEl = document.querySelector('.mdc-drawer .mdc-list');
const mainContentEl = document.querySelector('.main-content');
const tabBar = new MDCTabBar(document.querySelector('.mdc-tab-bar'));
const tab = new MDCTab(document.querySelector('.mdc-tab'));
const iconButtonRipple = new MDCRipple(document.querySelector('.mdc-icon-button-top-bar'));

iconButtonRipple.unbounded = true;
console.log("template");
listEl.addEventListener('click', (event) => {
  drawer.open = false;
});

document.body.addEventListener('MDCDrawer:closed', () => {
    mainContentEl.querySelector('input, button').focus();
  });

document.querySelector('.mdc-top-app-bar__navigation-icon').addEventListener('click', () => {
    drawer.open = true;
});


//map
var mymap = L.map('mapid').setView([50.8796, 4.7009], 13);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiY2VkcmljZHMyNTEyIiwiYSI6ImNrZ3d2YThyYTBkd3gyeXBmejdpdmc0M3YifQ.hNXfrpIUH38P-owvohaiIg'
}).addTo(mymap);

//$(window).on("resize", function () { $("#mapid").height($(window).height()); map.invalidateSize(); }).trigger("resize");

tabBar.listen('MDCTabBar:activated', (activatedEvent) => {
  console.log(activatedEvent.detail.index);
  document.querySelectorAll('.find_snapp_body').forEach((element, index) => {
    if (index === activatedEvent.detail.index) {
      element.classList.remove('find_snapp_body--hidden');
    } else {
      element.classList.add('find_snapp_body--hidden');
    }
  });
  mymap.invalidateSize();
});

const myIcon = L.icon({
  iconUrl: './images/marker-icon.png'
});

var marker = L.marker([51.5, -0.09], {icon: myIcon}).addTo(mymap);


var places = require('places.js');
var placesAutocomplete = places({
  appId: 'plWRP55NSUEK',
  apiKey: 'b70bfab4ee2d93efd50f9d9f26ec7243',
  container: document.querySelector('#address')
});


placesAutocomplete.on('change', function(e) {
  mymap.setView([e.suggestion.latlng.lat, e.suggestion.latlng.lng], 13);
  //document.getElementById("address").value = e.suggestion.value;
  console.log(e.suggestion.latlng);
});

