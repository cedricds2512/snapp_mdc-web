Tabbar layout editing
=====================
 ```
.mdc-tab {
  @include mdc-tab-text-label-color(red);
  @include mdc-tab-icon-color(red);

  .mdc-tab__ripple {
    @include mdc-states(blue);
  }
}

.mdc-tab-indicator {
  @include mdc-tab-indicator-underline-color(red);
}

@import "@material/tab-bar/mdc-tab-bar";
@import "@material/tab-scroller/mdc-tab-scroller";
@import "@material/tab-indicator/mdc-tab-indicator";
@import "@material/tab/mdc-tab";

```

Drawer layout editing
=====================
```
.mdc-drawer{
  @include drawer.divider-color(primary);
  @include drawer.fill-color-accessible(secondary)
}
```
Oude searchbar
==============
```
          <div class="searchbar-container-find_snapp">

            <button class="mdc-icon-button material-icons">gps_fixed</button>

            <label class="mdc-text-field mdc-text-field--filled searchbar-find_snapp">
              <span class="mdc-text-field__ripple"></span>
              <!-- <span class="mdc-floating-label" id="my-label-id">Search a location...</span> -->
              <input class="mdc-text-field__input" type="search" placeholder="Search a location ...">
              <span class="mdc-line-ripple"></span>
            </label>
          </div> 

          .searchbar-container-find_snapp{
            position: absolute;
            display: flex;
            justify-content: center;
            top: 120px;
            left: 0;
            right: 0;
            margin-left: auto;
            margin-right: auto;
            max-width: 250px;
            background-color: #e2f3d9ce;
            border-radius: 20px;
            border-style: solid;
            border-width: 1px;
            border-color: #63c132;
            z-index: 10;
            padding: 5px;

            .mdc-icon-button{
              margin-top: 4px;
              margin-right: 10px;
              @include icon-button.density(-4);
            }
          }

          .searchbar-find_snapp{
            @include textfield.density(-4);
            @include textfield.shape-radius(10px);
            @include textfield.fill-color(#00000000);
            @include textfield.placeholder-color($primary_color_dark);
            z-index: 12;
            max-width: 175px;
          }

          .mdc-text-field__input::-webkit-input-placeholder{
            opacity: 1;
          }
      ```

      Moving marker leaflet
      https://stackoverflow.com/questions/17875438/leafletjs-markers-move-on-zoom#:~:text=when%20your%20markers%20are%20moving,var%20locationIcon%20%3D%20L.