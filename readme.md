SnAPP using MDC_web
===================

__Step 1__
If you haven't already, please install node.js

__step 2__
Run the `npm install` command, to install all necessary dependencies (including MDC)

__step 3__
if you call `npm start`, a development server will start.
if you call `npm run build`, all files will be build to the dist folder

__step 4__
if you add a js or an scss file you have to import it into app.scss or app.js these are the entrypoints for webpack.

***
Precompiled version
===================

a precompiled version of the code can be found in the dist folder there you can checkout a little demo of the site.