const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: {
    template: ['./app.scss', './app.js'],
    find_snapp: ['./find_snapp.scss', './find_snapp.js']
  },
  output: {
    filename: '[name]-bundle.js',
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'template',
      filename: 'template.html',
      template: './template.html',
      chunks: ['template']
    }),
    new HtmlWebpackPlugin({
      title: 'find_snapp',
      filename: 'find_snapp.html',
      template: './find_snapp.html',
      chunks: ['find_snapp']
    })
  ],
  module: {
    rules: [
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
            }
          },
        ]
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].bundle.css',
            },
          },
          {loader: 'extract-loader'},
          {loader: 'css-loader'},
          {
            loader: 'sass-loader',
            options: {
              // Prefer Dart Sass
              implementation: require('sass'),

              // See https://github.com/webpack-contrib/sass-loader/issues/804
              webpackImporter: false,
              sassOptions: {
                includePaths: ['./node_modules'],
              },
            },
          }
        ],
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        query: {
          presets: ['@babel/preset-env'],
        },
      },
      {
        test: /\.(png|svg|jpg|gif|ico|html)$/,
        use: ['file-loader']
      }
    ],
  },
};